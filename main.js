const IOTA = require('iota.lib.js')
const converter = require('@iota/converter')

//Initialize variables
const nodeAddress = 'https://my-iota-node.com:14267'
let messageString = ''

// Create IOTA instance
var iota = new IOTA({
  provider: nodeAddress
});

//Expose node info
iota.api.getNodeInfo((err, res) => {
  if (err) {
    document.getElementById('node-result').innerText = JSON.stringify(err)
  } else {
    document.getElementById('node-app-name').innerText = "Application: " + res.appName;
    document.getElementById('node-app-version').innerText = "Version: " + res.appVersion;
    document.getElementById('node-neighbors').innerText = "Neighbors: " + res.neighbors;
    document.getElementById('node-time').innerText = "Time: " + new Date(res.time);
  }
})

//Trytes conversion - Here the explanation of each field: https://docs.iota.org/introduction/iota-token/anatomy-of-a-transaction
document.getElementById('get-trytes-conversion-btn').addEventListener('click', () => {
  let transactionTrytes = document.getElementById('tx-trytes').value
  txObject = iota.utils.transactionObject(transactionTrytes)
  console.log(txObject)
  txHashInTrits = converter.trits(txObject['hash'])
  let txHashTritsInString = ''
  //Convert TX message Tryte in ASCII (removing last character)
  let oldMessageTrytes = txObject['signatureMessageFragment']
  let newMessageTrytes = oldMessageTrytes.slice(0, oldMessageTrytes.length-1);
  messageString = converter.trytesToAscii(newMessageTrytes)
  for (let i=229; i<txHashInTrits.length; i++){
    //console.log(txHashInTrits[i])
    txHashTritsInString+=String(txHashInTrits[i])
    txHashTritsInString+=' | '
  }
  console.log("Last 14 digits in trits: " + txHashTritsInString)
  console.log(txHashInTrits)
  document.getElementById('tx-hash').innerText = "Tx hash: " + txObject['hash'];
  document.getElementById('bundle-hash').innerText = "Bundle hash: " + txObject['bundle'];
  document.getElementById('address').innerText = "Address: " + txObject['address'];
  document.getElementById('value').innerText = "Tx value: " + txObject['value'];
  document.getElementById('obsoleteTag').innerText = "Obsolete Tag: " + txObject['obsoleteTag'];
  document.getElementById('tag').innerText = "Tag: " + txObject['tag'];
  document.getElementById('trunkTransaction-hash').innerText = "Trunk Transaction hash: " + txObject['trunkTransaction'];
  document.getElementById('branchTransaction-hash').innerText = "Branch Transaction hash: " + txObject['branchTransaction'];
  document.getElementById('timestamp').innerText = "Timestamp: " + new Date(txObject['timestamp'] * 1000);
  document.getElementById('attachmentTimestamp').innerText = "Attachment Timestamp: " + new Date(txObject['attachmentTimestamp']);
  document.getElementById('attachmentTimestampLowerBound').innerText = "Attachment Timestamp Lower Bound: " + txObject['attachmentTimestampLowerBound'];
  document.getElementById('attachmentTimestampUpperBound').innerText = "Attachment Timestamp Upper Bound: " + txObject['attachmentTimestampUpperBound'];
  document.getElementById('currentIndex').innerText = "Current Index: " + txObject['currentIndex'];
  document.getElementById('lastIndex').innerText = "Last Index: " + txObject['lastIndex'];
  document.getElementById('nonce').innerText = "Nonce: " + txObject['nonce'];
  document.getElementById('message').innerText = "Message: " + messageString;
  document.getElementById('tx-hash-in-trits').innerText = "Last 14 digits of TX HASH in Trits: " + txHashTritsInString;
})